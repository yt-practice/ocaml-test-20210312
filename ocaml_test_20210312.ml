let fizzbuzz n =
  if n mod 15 = 0 then "fizzbuzz"
  else if n mod 3 = 0 then "fizz"
  else if n mod 5 = 0 then "buzz"
  else string_of_int n;;

let rec loop n top =
  if n > top then ()
  else begin
    print_endline (fizzbuzz n);
    loop (n + 1) top;
  end;;

let fizzbuzz_lines n =
  loop 1 n;;

let () = fizzbuzz_lines 20;;
