# ocaml を使ってみるテスト

## set up

```shell
opam install --locked .
```

## build

```shell
dune build
```

## run builded

```shell
dune exec ./ocaml_test_20210312.exe
```
